#######################################################
# 
#   Multi Arm Spirograph
#   Author: Eugene Bolshakov
#   30.03.2019
# 
#######################################################
#
#   Rotor - Point that rotates at given speed with given radius.
#           Uses Polar coordinate system as basis (a,r)
#   
#   Spirograph - Uses array of Rotors, and outputs last point position in decartes coordinates (x,y)
#
#
#######################################################



from Rotor import Rotor
import math
import random
import os
import sys
ROOTPATH=os.path.dirname(sys.modules['__main__'].__file__)

import pickle
aa=str(pickle.dumps([1,2,3]))

#importing for window to not show



class Spirograph:
    m_Rotor= Rotor()
    RotorList=[]
    Ratios=[]
    Results=[]
    def __init__(self):
        # m_Rotor is the origin rotor of the system.
        # all other rotors must be added from outside
        self.Reset()
        #self.AddRotor(self.m_Rotor,1)
    
    def Reset(self):
        self.m_Rotor= Rotor()
        self.RotorList=[]
        self.Ratios=[]
        self.Results=[]
        self.AddRotor(self.m_Rotor,1)
        
    def ToZero(self):
        self.Results=[]
        for r in self.RotorList:
            r.ToZero()
    def LoadFromStr(self,st):
        unp=pickle.loads(st)
        #unp[0] #create rotors for each list
        self.Reset()
        self.Ratios=unp[1]
        self.RotorList=[]
        for rotv in unp[0]:
            self.RotorList.append(Rotor(rotv[2],rotv[1])) #angle , offset, radius

        return unp
        
    def SaveToStr(self):
        Rr=[x.GetVals() for x in self.RotorList]
        ar=[Rr,self.Ratios]

        return pickle.dumps(ar)

    def AddRotor(self,rotor,ratio):
        self.RotorList.append(rotor)
        self.Ratios.append(ratio)
        
    def AddRotor2(self,rad,offset,ratio):
        self.RotorList.append(Rotor(rad,offset))
        self.Ratios.append(ratio)
    
    def DoStep(self,step):   #Advance system one step ahead, Output [x,y] of this step.
        #On each rotor apply step modified by ratio
        outxy=[0,0]
        for pair in zip(self.RotorList,self.Ratios):
            step=step*pair[1]  #propogate angle step to next rotor.
            out=pair[0].DoStep(step)
            outxy[0]+=pair[0].x  # offset rotor to new origin.
            outxy[1]+=pair[0].y
       
        return outxy

    def Iterate(self,nsteps,da):  #Do nsteps where da = delta angle (radians)
        # Add all points to internal Results array.
        self.Results=[]
        for i in range(nsteps):
            res=self.DoStep(da)
            self.Results.append(res)
            
    def IterateCircle(self,ncircles,da): 
        return self.Iterate(math.ceil((ncircles*pi*2)/da),da)

    def GetMaxLen(self):
        l=0
        for rot in self.RotorList:
            l=l+math.fabs(rot.GetRad())
        return l
    
    def GetMaxSpeed(self,da):  #incomplete
        
        maxarch=self.GetMaxLen()*da
        maxvel=maxarch
        
        for pair in zip(self.RotorList,self.Ratios):
            curdist=2*math.pi*pair[0].GetRad()*pair[1]*da #(2*pi*R) * (ratio*da)
            maxvel+=curdist
            
        
            
    
    def RenderOutput(self,ncircles,step,imgname,pxSide):
        self.IterateCircle(ncircles,step)
        x=[]
        y=[]
        x=[i[0] for i in self.Results]
        y=[i[1] for i in self.Results]
        
        fig, ax = plt.subplots()
        #ax.plot(x, y, linewidth=3, alpha=0.3, solid_capstyle="butt")
        ax.scatter(x, y, s=20, alpha=0.1,edgecolors='none')
        ax.set_axis_off()

        maxsize=self.GetMaxLen()+1
        fig.set_size_inches(10, 10)
        #fig.tight_layout()
        plt.xlim(-maxsize, maxsize)
        plt.ylim(-maxsize, maxsize)

        if imgname[-3:].lower()=="png":
            fig.savefig(imgname, dpi=pxSide/10) #side = dpi*inches ; 50*10=500
        elif imgname[-3:].lower()=="svg":
            fig.savefig(imgname) #side = dpi*inches ; 50*10=500
            
        plt.close(fig)
        return 1    

    def ShowPic(self,ncircles,step):
        self.IterateCircle(ncircles,step)
        x=[]
        y=[]
        x=[i[0] for i in self.Results]
        y=[i[1] for i in self.Results]
        
        fig, ax = plt.subplots()
        #ax.plot(x, y, linewidth=3, alpha=0.3, solid_capstyle="butt")
        ax.scatter(x, y, s=20, alpha=0.1,edgecolors='none')
        ax.set_axis_off()

        maxsize=self.GetMaxLen()+1
        #fig.tight_layout()
        plt.xlim(-maxsize, maxsize)
        plt.ylim(-maxsize, maxsize)

        return 1  
    
    
    def SavePic(self,ncircles,step,imgname):
        self.IterateCircle(ncircles,step)
        x=[]
        y=[]
        x=[i[0] for i in self.Results]
        y=[i[1] for i in self.Results]
        
        fig, ax = plt.subplots()
        #ax.plot(x, y, linewidth=3, solid_capstyle="butt")
        ax.scatter(x, y, s=50, alpha=0.1,edgecolors='none')
        #ax.scatter(x, y)
        #plt.style.use('classic')
        #plt.rcParams['alpha']
        ax.set_axis_off()
        
        matplotlib.get_configdir()
        
        maxsize=self.GetMaxLen()+1
        fig.set_size_inches(10, 10)
        fig.tight_layout()
        plt.xlim(-maxsize, maxsize)
        plt.ylim(-maxsize, maxsize)
        fig.savefig(imgname, dpi=50) #side = dpi*inches ; 50*10=500
        plt.close(fig)
        return 1

    def GetXY(self):
        return 1
    



S=Spirograph()
S.Reset()
S.m_Rotor.SetRad(10)

R2=Rotor(9,3.1415)
S.AddRotor(R2,2.2)

sst=S.SaveToStr()
S.LoadFromStr(sst)

S.RotorList



S.IterateCircle(7,0.001)

x=[i[0] for i in S.Results]
y=[i[1] for i in S.Results]

fig, ax = plt.subplots()
ax.plot(x, y)

ax.set(xlabel='X', ylabel='Y',
       title='Spirograph draw')
ax.grid()


maxsize=S.GetMaxLen()+5
fig.savefig("test.png")
plt.xlim(-maxsize, maxsize)
plt.ylim(-maxsize, maxsize)
plt.autoscale(enable=False, axis='both', tight=False)

#plt.show()
plt.close(fig)

######################################## GUI part ##########################################

from PyQt5 import QtWidgets,QtCore,QtGui
from PyQt5.QtWidgets import QApplication

from mainspiro import Ui_MainWindow

class mywindow(QtWidgets.QMainWindow):
    midRatio=1
    rat2step=1
    rad2=7
    ratio2=2
    off2=0
    
    rad3=3
    ratio3=2
    off3=0
    
    circles=8
    
    SpiroL=Spirograph()
    SpiroC=Spirograph()
    SpiroR=Spirograph()
    
    def __init__(self):
        super(mywindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        

        
        # подключение клик-сигнал к слоту btnClicked
        self.ui.btnBuild.clicked.connect(self.btnStartClicked)
        
        self.ui.btnRight.clicked.connect(self.gvRightClicked)
        self.ui.btnLeft.clicked.connect(self.gvLeftClicked)
        self.ui.btnViewOnly.clicked.connect(self.showPlot)
        self.ui.btnRenderPng.clicked.connect(self.saveFileDialogPng)
        self.ui.btnRenderSvg.clicked.connect(self.saveFileDialogSvg)
        #self.ui.gvRight.mousePressEvent.connect(self.gvRightClicked)
        
    def saveFileDialogPng(self):    
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getSaveFileName(self,"QFileDialog.getSaveFileName()","","Raster PNG (*.png)", options=options)
        if fileName:
            print(fileName)
            if fileName[-3:].lower()!='png':
                fileName=fileName+'.png'
            self.renderCenter(fileName)

    def saveFileDialogSvg(self):    
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getSaveFileName(self,"QFileDialog.getSaveFileName()","","Vector SVG (*.svg)", options=options)
        if fileName:
            if fileName[-3:].lower()!='svg':
                fileName=fileName+'.svg'
            self.renderCenter(fileName)



    def gvRightClicked(self):
        newratio=self.ui.sbRatio2.value()+self.rat2step
        self.ui.sbRatio2.setValue(newratio)
        self.ratio2=newratio

        self.btnStartClicked()
    def gvLeftClicked(self):
        newratio=self.ui.sbRatio2.value()-self.rat2step
        self.ui.sbRatio2.setValue(newratio)
        self.ratio2=newratio

        self.btnStartClicked()

    def showPlot(self):
        
        self.rad2=self.ui.sbRad2.value()
        self.ratio2=self.ui.sbRatio2.value()
        self.rad3=self.ui.sbRad3.value()
        self.ratio3=self.ui.sbRatio3.value()
        
        self.circles=self.ui.sbCircles.value()
        self.da=self.ui.sbDeltaA.value()
        
        #generating images:
        #central
        S=Spirograph()
        S.Reset()
        S.m_Rotor.SetRad(10)
        R2=Rotor(self.rad2,self.off2)
        S.AddRotor(R2,self.ratio2)
        
        R3=Rotor(self.rad3,self.off3)
        S.AddRotor(R3,self.ratio3)


        #S.SavePic(circles,0.01,'testC.png')
        S.ShowPic(self.circles,self.da)
        
    def renderCenter(self,fname):
        
        self.rad2=self.ui.sbRad2.value()
        self.ratio2=self.ui.sbRatio2.value()
        self.rad3=self.ui.sbRad3.value()
        self.ratio3=self.ui.sbRatio3.value()
        
        self.circles=self.ui.sbCircles.value()
        self.da=self.ui.sbDeltaA.value()
        
        #generating images:
        #central
        S=Spirograph()
        S.Reset()
        S.m_Rotor.SetRad(10)
        R2=Rotor(self.rad2,self.off2)
        S.AddRotor(R2,self.ratio2)
        
        R3=Rotor(self.rad3,self.off3)
        S.AddRotor(R3,self.ratio3)
        print(S.RotorList[2].GetVals())
        print(S.Ratios)
        #S.SavePic(circles,0.01,'testC.png')
        S.RenderOutput(self.circles,self.da,fname,self.ui.sbResolution.value())
        
    
    def btnStartClicked(self):
    
        sqsize=500
        
        self.sceneC = QtWidgets.QGraphicsScene()
        self.ui.gvCentral.setScene(self.sceneC)
        
        self.ui.gvCentral.fitInView(0,0,sqsize,sqsize)
        
        self.sceneL = QtWidgets.QGraphicsScene()
        self.ui.gvLeft.setScene(self.sceneL)
        self.ui.gvLeft.fitInView(0,0,sqsize,sqsize)
        
        self.sceneR = QtWidgets.QGraphicsScene()
        self.ui.gvRight.setScene(self.sceneR)
        self.ui.gvRight.fitInView(0,0,sqsize,sqsize)
        
        circles=8
        
        self.rad1=self.ui.sbRad1.value()
        self.rad2=self.ui.sbRad2.value()
        self.ratio2=self.ui.sbRatio2.value()
        self.off2=self.ui.sbOff2.value()*math.pi*2
        
        
        self.rad3=self.ui.sbRad3.value()
        self.ratio3=self.ui.sbRatio3.value()
        self.off3=self.ui.sbOff3.value()*math.pi*2
        
        self.circles=self.ui.sbCircles.value()
        self.da=self.ui.sbDeltaA.value()
        
        #generating images:
        #central
        S=Spirograph()
        S.Reset()
        S.m_Rotor.SetRad(self.rad1)
        
        #R2=Rotor(self.rad2,self.off2)
        #S.AddRotor(R2,self.ratio2)
        
        S.AddRotor2(self.rad2,self.off2,self.ratio2)     
        S.AddRotor2(self.rad3,self.off3,self.ratio3)
        
        S.SavePic(self.circles,self.da,'testC.png')
        
        #right
        S.ToZero()
        S.Ratios[1]=self.ratio2+self.rat2step
        S.SavePic(self.circles,self.da,'testR.png')
        
        #left
        S.ToZero()
        S.Ratios[1]=self.ratio2-self.rat2step
        S.SavePic(self.circles,self.da,'testL.png')

        pixmapC = QtGui.QPixmap(ROOTPATH+"/testC.png")
        pixmapR = QtGui.QPixmap(ROOTPATH+"/testR.png")
        pixmapL = QtGui.QPixmap(ROOTPATH+"/testL.png")
        
        self.sceneC.clear()
        self.sceneR.clear()
        self.sceneL.clear()
        
        self.sceneC.addPixmap(pixmapC)
        self.sceneR.addPixmap(pixmapR)
        self.sceneL.addPixmap(pixmapL)
        plt.close(fig)
   
        #self.renderCenter()
        self.setFocus()
        
        
  

app = QtCore.QCoreApplication.instance()
if app is None:
    app = QtWidgets.QApplication(sys.argv)
    
application = mywindow()

application.show()
application.btnStartClicked()

sys.exit(app.exec())



