# MetaSpirograph

Meta spirograph with unlimited sub-circles

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Лицензия Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />Это произведение доступно по <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">лицензии Creative Commons «Attribution» («Атрибуция») 4.0 Всемирная</a>.


Recommended: 

use latest Anaconda on python 3.7

Run from file: Spirograph.py


Video demonstration: https://www.youtube.com/watch?v=b9AMqkFHcqg

(C) 2019 Eugene Bolshakov


